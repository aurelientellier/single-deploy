
Fichier unique permettant de réaliser différentes opérations sur les environnements PHP.


ATTENTION : il est nécessaire de bien renseigner les paramètres en en-tête de fichier.

# Actions

## ``directory_delete`` Suppression d'un répertoire

### Paramètres : 
- ``directory_name`` : Nom du répertoire à supprimer.

### Exemple :
```
 tools.php?action=directory_delete&directory_name=monrep/sousrep
```
### Contrôles
On vérifie que le répertoire à supprimer n'est pas un sous-répertoire du répertoire racine

## ``directory_archive`` : Archivage d'un répertoire au format zip

### Description
Cette action permet de zipper le contenu d'un répertoire et de spécifier l'emplacement de cette sauvegarde.

Le fichier sauvegardé aura comme nom ``<nom repertoire à archiver>.backup-<date AAAA-MM-JJ_hh-mm-ss>.zip``

## Paramètres
- directory_path : chemin du répertoire à archiver.
- archive_path : chemin de répertoire dans lequel mettre l'archive.

## ``directory_duplicate`` : Duplication d'un répertoire

### Description
Cette action permet de dupliquer un répertoire et ainsi d'en faire une copie.

### Paramètres :
- ``directory_name`` : Chemin complet du répertoire à duppliquer
- ``destination_name`` : Chemin complet du répertoire dupliqué. Ce répertoire ne doit pas déjà exister

### Exemple :
``` 
 tools.php?action=directory_name&directory_name=monrep/sousrep&destination_name=monrep/sousrepcopy
```
